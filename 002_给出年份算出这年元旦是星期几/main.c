#include <stdio.h>
int
main() {
    int a, s, z;
    printf("请输入年份yyyy: ");
    scanf("%d", &a);
    s = a + (a - 1) / 4 - (a - 1) / 100 + (a - 1) / 400;
    z = s % 7;
    if (z == 0) printf("元旦这天是星期日\n");
    if (z == 1) printf("元旦这天是星期一\n");
    if (z == 2) printf("元旦这天是星期二\n");
    if (z == 3) printf("元旦这天是星期三\n");
    if (z == 4) printf("元旦这天是星期四\n");
    if (z == 5) printf("元旦这天是星期五\n");
    if (z == 6) printf("元旦这天是星期六\n");
}