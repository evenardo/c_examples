#include <stdio.h>

struct date {
    int year;
    int month;
    int day;
};

int
run_year(int year) {
    if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
        return 1;
    else
        return 0;
}

int
count_days(struct date current_day) {
    int per_month[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int total_day = 0, year, i;
    for (year = 1990; year < current_day.year; year++) {
        if (run_year(year))
            total_day = total_day + 366;
        else
            total_day = total_day + 365;
    }
    if (run_year(current_day.year)) per_month[2] += 1;
    for (i = 0; i < current_day.month; i++) total_day += per_month[i];
    total_day += current_day.day;
    return (total_day);
}

int
main() {
    struct date today;
    int total_day;
    printf("请输入指定年月日(yyyy,mm,dd): ");
    scanf("%d,%d,%d", &today.year, &today.month, &today.day);
    total_day = count_days(today) - 1;
    printf("总天数是%d天\n", total_day);
    printf("总小时数是%d小时\n", 24 * total_day);
    return 0;
}