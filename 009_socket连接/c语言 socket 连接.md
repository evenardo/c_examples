
## c 语言 socket 连接

#### 建立连接的步骤
  * 用 socket() 创建一个socket
  * 返回的是 socket() 描述符

#### int socket( int domain,int type, int protocol );

#### int domain 
  * 用来选择协议簇
  * PF_UNIX
  * PF_LOCAL
  * PF_FILE
  * 这三个代表的数字一模一样,都代表本地通讯.
  * PF_INET 代表ipv4
  * PF_INET6 代表ipv6

#### int type 
  * 用于选择通讯类型
  * SOCK_STREAM -> 数据流 (tcp)       
  * SOCK_DGRAM -> 数据报 (udp)

#### int protocol 
  * 已经没有意义了
  * 用于定义协议类型
  * 第二个位置已经定义了,现在没什么好定义的了.

#### 返回值
  * socket 描述符
  * 失败返回 -1

#### struct sockaddr
  * 不存数据 专门做参数类型

#### struct sockaddr_un
  * 存储本地通讯数据

#### socket 文件
  * 本地通讯使用的是一个IPC文件做媒介
  * 文件后缀是 .sock

#### struct sockaddr_in
``` c
struct sockaddr_in { 
    short int         sin_family;
    unsigned short    int sin_port; 
    struct in_addr    sin_addr;

    unsigned char     sin_zero[8];
}

struct in_addr    { 
    unsigned long s_addr;
}
``` 

#### struct sockaddr_un
``` c
struct sockaddr_un {
    sa_family_t sun_family;     /* AF_UNIX */
    char        sun_path[108];  /* pathname */
};
```
#### 第三步 绑定 bind

  * bind(sockfa,sockaddr,sizeof(addr))

#### 第四步 通讯
  * 用文件的 read write 等函数


