## nc 使用例子

#### 用这个命令可以从一个服务器发送文件至另一个服务器

  * 服务端:  
    dd if=/dev/zero bs=1MB count=1000 |nc [address] [port] 

  * 客户端:  
    nc -l 5001 | pv -W > /dev/nul342l
