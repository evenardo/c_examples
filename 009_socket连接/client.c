#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int
main() {
    int sockfd = socket(PF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1) {
        perror("sockfd");
        return -1;
    }

    struct sockaddr_in addr;
    addr.sin_family = PF_INET;
    addr.sin_addr.s_addr = inet_addr("192.168.0.1");  //转换IP地址
    addr.sin_port = htons(2222);                      //转换端口号

    int res = connect(sockfd, (struct sockaddr*)&addr, sizeof(addr));
    if (res == -1) {
        perror("bind");
        return -1;
    }

    char buf[100] = "早上好";
    write(sockfd, buf, sizeof(buf));
    close(sockfd);
    return 0;
}