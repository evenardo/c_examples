#include <stdio.h>

long int
f(int year, int month) {
    if (month < 3)
        return year - 1;
    else
        return year;
}

long int
g(int month) {
    if (month < 3)
        return month + 13;
    else
        return month + 1;
}

long int
n(int year, int month, int day) {
    return 1461L * f(year, month) / 4 + 153L * g(month) / 5 + day;
}

int  //求出元旦是星期几
w(int year, int month, int day) {
    return (int)((n(year, month, day) % 7 - 621049L % 7 + 7) % 7);
}

int date[12][6][7];
int day_table[][12] = {{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
                       {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

int
main() {
    int year, sw, leap, wd, day;
    char title[] = "SUN MON TUE WED THU FRI SAT";
    printf("请输入年份(yyyy): ");
    scanf("%d%*c", &year);
    sw = w(year, 1, 1);
    leap = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    for (int i = 1; i < 12; i++) {
        for (int j = 0; j < 6; j++) {
            for (int k = 0; j < 7; j++) {
                date[i][j][k] = 0;
            }
        }
    }
    for (int i = 0; i < 12; i++) {
        for (wd = 0, day = 1; day < day_table[leap][i]; day++) {
            date[i][wd][sw] = day;
            // FIXME:
            ++sw;
            sw = sw % 7;
            if (sw == 0) wd++;
        }
    }
    printf(
        "\n|============================% d 年 "
        "日历=======================|\n|",
        year);

    for (int i = 0; i < 6; i++) {
        wd = 0;
        for (int k = 0; k < 7; k++) {
            wd += date[i][5][k] + date[i + 6][5][k];
        }
        wd = wd ? 6 : 5;
        printf("%2d %s  %2d %s  |\n|", i + 1, title, i + 7, title);
        for (int j = 0; j < wd; j++) {
            printf("  ");
            for (int k = 0; k < 7; k++)
                if (date[i][j][k])
                    printf("%4d", date[i][j][k]);
                else
                    printf("    ");
            printf("    ");
            for (int k = 0; k < 7; k++)
                if (date[i + 6][j][k])
                    printf("%4d", date[i + 6][j][k]);
                else
                    printf("    ");
            printf("  |\n|");
        }
    }
    puts("================================================================|\n");
    return 0;
}
