#include <stdio.h>
#include <string.h>

#define MAX 1000

void
input_number(int *num_1, int *num_2) {
    char temp[MAX];  //用于存放输入的数据
    int len;
    memset(temp, 0, sizeof(temp));
    scanf("%s", temp);  //清空容器并输入第一个数据
    len = strlen(temp);
    printf("%d\n", len);
    num_1[0] = len;
    printf("%d\n", num_1[0]);
    for (int i = len; i > 0; i--) {
        num_1[i] = temp[num_1[0] - i] - '0';  //逆序并转换成相应的数字
    }
    memset(temp, 0, sizeof(temp));
    scanf("%s", temp);  //清空容器并输入第二个数据
    len = strlen(temp);
    num_2[0] = len;
    for (int i = len; i > 0; i--)
        num_2[i] = temp[num_2[0] - i] - '0';  //逆序并转换成相应的数字
}

int
max(int x, int y) {
    return x > y ? x : y;
}

void
add_number(int *num_1, int *num_2, int *sum) {
    sum[0] = max(num_1[0], num_2[0]);
    for (int i = 1; i <= sum[0]; i++)  //相加至长度最大的那个才结束
    {
        sum[i] += num_1[i] + num_2[i];  //计算相加
        sum[i + 1] = sum[i] / 10;       //把进位的数值留到下一位
        sum[i] %= 10;
    }
}

void
output_number(int *sum) {
    for (int i = ++sum[0]; !sum[i] && 1 < i; sum[0]--, i--)
        ;  //长度增加1，比如99+1==100，就是3位长度；过滤前导0；避免结果全部是0，所以只留下一位不过滤
    for (int i = sum[0]; i > 0; i--) printf("%d", (int)sum[i]);  //输出结果
    printf("%s", "\n\n");
}

int
main(void) {
    while (1) {
        int num_1[MAX], num_2[MAX], num_3[MAX];
        memset(&num_1, 0, sizeof(num_1));  //存放加数，并清空
        memset(&num_2, 0, sizeof(num_2));  //存放加数，并清空
        memset(&num_3, 0, sizeof(num_3));  //存放结果，并清空
        input_number(num_1, num_2);        //输入2个加数
        add_number(num_1, num_2, num_3);   //计算相加
        output_number(num_3);              //输出结果
    }
    return 0;
}