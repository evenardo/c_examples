#include <cstring>
#include <iostream>

#define MAX 1000

using namespace std;

void
input_number(char *num_1, char *num_2) {
    char *temp = new char[MAX];  //用于存放输入的数据
    memset(temp, 0, sizeof(temp));
    cin >> temp;  //清空容器并输入第一个数据
    for (int i = num_1[0] = strlen(temp); i > 0; i--)
        num_1[i] = temp[num_1[0] - i] - '0';  //逆序并转换成相应的数字
    memset(temp, 0, sizeof(temp));
    cin >> temp;  //清空容器并输入第二个数据
    for (int i = num_2[0] = strlen(temp); i > 0; i--)
        num_2[i] = temp[num_2[0] - i] - '0';  //逆序并转换成相应的数字

    delete[] temp;  //释放临时容器
}

void
add_number(char *num_1, char *num_2, char *sum) {
    sum[0] = max(num_1[0], num_2[0]);
    for (int i = 1; i <= sum[0]; i++)  //相加至长度最大的那个才结束
    {
        sum[i] += num_1[i] + num_2[i];  //计算相加
        sum[i + 1] = sum[i] / 10;       //把进位的数值留到下一位
        sum[i] %= 10;
    }
}

void
output_number(char *sum) {
    for (int i = ++sum[0]; !sum[i] && 1 < i; sum[0]--, i--)
        ;  //长度增加1，比如99+1==100，就是3位长度；过滤前导0；避免结果全部是0，所以只留下一位不过滤
    for (int i = sum[0]; i > 0; i--) cout << (int)sum[i];  //输出结果
    cout << endl << endl;
}

int
main(void) {
    while (true) {
        char *num_1 = new char[MAX];
        memset(&num_1, 0, sizeof(num_1));  //存放加数，并清空
        char *num_2 = new char[MAX];
        memset(&num_2, 0, sizeof(num_2));  //存放加数，并清空
        char *num_3 = new char[MAX];
        memset(&num_3, 0, sizeof(num_3));  //存放结果，并清空

        InputNumber(num_1, num_2);       //输入2个加数
        AddNumber(num_1, num_2, num_3);  //计算相加
        OutputNumber(num_3);             //输出结果

        delete[] num_1;  //回收对象
        delete[] num_2;
        delete[] num_3;
    }
    return 0;
}