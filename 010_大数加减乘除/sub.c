#include <stdio.h>
#include <string.h>

#define MAX 400

int
main() {
    /*读取第一个数*/
    char m[MAX] = {0};
    scanf("%s", m);
    /*获取第一个数的长度*/
    int len_m = strlen(m);
    /*将获取的字符串逆序并且转换为十进制整数保存*/
    int a[MAX] = {0};
    for (int i = 0; i < len_m; i++) {
        a[i] = m[len_m - 1 - i] - '0';
    }
    /*读取第二个数*/
    char n[MAX] = {0};
    scanf("%s", n);
    /*获取第二个数的长度*/
    int len_n = strlen(n);
    /*将获取的字符串逆序并且转换为十进制整数*/
    int b[MAX] = {0};
    for (int i = 0; i < len_n; i++) {
        b[i] = n[len_n - 1 - i] - '0';
    }
    /*打印结果 如果第一个数的长度第二个数长*/
    if (len_m < len_n) {
        /*打印一个负号*/
        printf("-");
        /*计算十进制每一位 按最长的数字长度循环*/
        for (int i = 0; i < len_n; i++) {
            b[i] -= a[i];
            /*如果是负数则借位*/
            if (b[i] < 0) {
                b[i] += 10;
                b[i + 1]--;
            }
        }
        /*为了打印每一位去掉高位的0不打印*/
        int i = len_n - 1;
        while (b[i] == 0) i--;
        for (; i >= 0; i--) {
            printf("%d", b[i]);
        }
    } else {
        /*第一个数不比第二个数长*/
        /*计算十进制每一位 按最长的数字长度循环*/
        for (int i = 0; i < len_m; i++) {
            a[i] -= b[i];
            /*如果是负数则借位*/
            if (a[i] < 0) {
                a[i] += 10;
                a[i + 1]--;
            }
        }
        /*为了打印每一位 去掉高位的0不打印*/
        int i = len_m - 1;
        while (a[i] == 0) i--;
        for (; i >= 0; i--) {
            printf("%d", a[i]);
        }
    }
    /*打印一个换行*/
    printf("\n");
    /*执行完毕 成功返回 0*/
    return 0;
}