#include <stdio.h>
#include <string.h>

#define MAX 1000

int
main() {
    /*读取第一个数*/
    char m[MAX] = {0};
    scanf("%s", m);
    /*获取字符串的长度*/
    const int len_m = strlen(m);
    /*将读取的字符串反转每一位读入a[]中并转换为十进制数字*/
    int a[MAX] = {0};
    for (int i = 0; i < len_m; i++) {
        a[i] = m[len_m - 1 - i] - '0';
    }
    /*读取第二个数*/
    char n[MAX] = {0};
    scanf("%s", n);
    /*获取第二个数的长度*/
    const int len_n = strlen(n);
    /*将第二个字符串的每位转为数字并逆序存入b[]*/
    int b[MAX] = {0};
    for (int i = 0; i < len_n; i++) {
        b[i] = n[len_n - 1 - i] - '0';
    }
    /*比较那个数比较长*/
    int len_max;
    if (len_m > len_n)
        len_max = len_m;
    else
        len_max = len_n;
    /*计算两个数十进制的每一位
      包括进位一并计算*/
    int carry = 0;
    char c[MAX] = {0};
    for (int i = 0; i < len_max; i++) {
        /*获得每一位的余数*/
        c[i] = (a[i] + b[i] + carry) % 10;
        /*获得进位数值*/
        carry = (a[i] + b[i] + carry) / 10;
    }
    /*判断最高位是否产生了进位
      如果产生了进位则让最高位等于进位值*/
    if (carry != 0) c[len_max] = carry;
    /*如果有进位则把进位打印出来*/
    if (c[len_max]) printf("%d", c[len_max]);
    /*把结果打印出来 从高位开始打印*/
    for (int i = len_max - 1; i >= 0; i--) {
        printf("%d", c[i]);
    }
    printf("\n");
    /*返回 成功 0 */
    return 0;
}